# DataGenerator
A little utility that generate most common data required by someone

## Generate data bellow
 - Binary raw data
 - Random string/text
 - Timestamp
 - UUID/GUID
 
## Download binaries
 - [All downloads](https://bitbucket.org/kelvinchin12070811/datagenerator/downloads/)
 - [Latest jar](https://bitbucket.org/kelvinchin12070811/datagenerator/downloads/DataGenerator_1.0.1.jar)
 - [Latest exe](https://bitbucket.org/kelvinchin12070811/datagenerator/downloads/DataGenerator_1.0.1.exe)