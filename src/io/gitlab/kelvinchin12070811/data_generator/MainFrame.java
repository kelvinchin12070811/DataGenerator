//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
//***********************************************************************************************************
//This Source Code Form is subject to the terms of the Mozilla Public
//License, v. 2.0. If a copy of the MPL was not distributed with this
//file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
import javax.swing.JTabbedPane;

import io.gitlab.kelvinchin12070811.data_generator.generator.*;
public class MainFrame extends JFrame
{
	private JTabbedPane tabs = null;
	
	public MainFrame()
	{
		tabs = new JTabbedPane();
		
		List<Generator> generators = new LinkedList<Generator>();
		generators.add(new TimeStampGenerator());
		generators.add(new UnixTimestampGenerator());
		generators.add(new UuidGenerator());
		generators.add(new StringGenerator());
		generators.add(new RawDataGenerator());
		
		for (Generator itr : generators)
			tabs.addTab(itr.getTabTitle(), itr);
		
		this.add(tabs);
		
		MenuBar menubar = new MenuBar();
		Menu help = new Menu("Help");
		
		MenuItem about = new MenuItem("About");
		about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Data generator is an experiment of Kelvin Chin");
			}
		});
		help.add(about);
		
		menubar.add(help);
		this.setMenuBar(menubar);
	}
}
