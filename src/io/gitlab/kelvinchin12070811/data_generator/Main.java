//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class Main
{	
	public static final String VERSION_NUM = "1.1.0";
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			var frame = new MainFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(458, 196);
			frame.setTitle(String.format("Data Generator v%s by Kelvin Chin", VERSION_NUM));
			frame.setIconImage(ImageIO.read(ClassLoader.getSystemResource("icon.png")));
			frame.setLocationRelativeTo(null);
			frame.setResizable(false);
			frame.setVisible(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
