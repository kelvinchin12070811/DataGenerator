//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator.generator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

public class TimeStampGenerator extends Generator
{
	private JButton genBtn = new JButton("Generate");
	
	private JLabel t1 = new JLabel("With seperator");
	private JLabel t2 = new JLabel("Without seperator");
	private JTextField t1Time = new JTextField();
	private JTextField t2Time = new JTextField();
	
	public TimeStampGenerator()
	{
		genBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		
		t1.setPreferredSize(new Dimension(125, 12));
		t2.setPreferredSize(new Dimension(125, 12));
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalStrut(Generator.OUT_BORDER_SIZE));
		
		var group1 = Box.createHorizontalBox();
		group1.add(t1);
		group1.add(t1Time);
		this.add(group1);
		this.add(Box.createVerticalStrut(Generator.HLINE_GAP));
		
		var group2 = Box.createHorizontalBox();
		group2.add(t2);
		group2.add(t2Time);
		this.add(group2);
		this.add(Box.createVerticalStrut(Generator.HLINE_GAP));
		
		var group3 = new JPanel();
		group3.setLayout(new BorderLayout(Generator.OUT_BORDER_SIZE, 0));
		group3.add(genBtn, BorderLayout.CENTER);
		this.add(group3);
		this.add(Box.createHorizontalStrut(Generator.OUT_BORDER_SIZE));
		
		generate();
	}
	
	private void generate()
	{
		LocalDateTime dt = LocalDateTime.now();
		String tm = dt.format(DateTimeFormatter.ISO_DATE_TIME);
		tm = tm.substring(0, tm.indexOf('.'));
		
		t1Time.setText(tm);
		t2Time.setText(tm.replaceAll(":", "").replaceAll("-", ""));
	}
	
	@Override
	public String getTabTitle() {
		return "Time stamp";
	}
}
