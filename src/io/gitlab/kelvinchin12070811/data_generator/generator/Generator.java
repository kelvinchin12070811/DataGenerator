//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator.generator;
import java.awt.Dimension;

import javax.swing.JPanel;

public abstract class Generator extends JPanel
{
	public static final int OUT_BORDER_SIZE = 5;
	public static final int HLINE_GAP = 12;
	public static final Dimension LABEL_DIM = new Dimension(125, 12);
	
	public abstract String getTabTitle();
}
