// ***********************************************************************************************************
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// ***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator.generator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneOffsetTransitionRule;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UnixTimestampGenerator extends Generator
{
    private JButton genBtn = new JButton("Generate");

    private JLabel t1 = new JLabel("Unix Time");
    private JTextField t1Time = new JTextField();

    public UnixTimestampGenerator()
    {
        genBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                generate();
            }
        });

        t1.setPreferredSize(new Dimension(125, 12));

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createVerticalStrut(Generator.OUT_BORDER_SIZE));

        var group1 = Box.createHorizontalBox();
        group1.add(t1);
        group1.add(t1Time);
        this.add(group1);
        this.add(Box.createVerticalStrut(Generator.HLINE_GAP));
        
        this.add(Box.createVerticalStrut(40));

        var group3 = new JPanel();
        group3.setLayout(new BorderLayout(Generator.OUT_BORDER_SIZE, 0));
        group3.add(genBtn, BorderLayout.CENTER);
        this.add(group3);
        this.add(Box.createHorizontalStrut(Generator.OUT_BORDER_SIZE));

        generate();
    }

    private void generate()
    {
        LocalDateTime dt = LocalDateTime.now();
        ZoneId zone = ZoneId.systemDefault();
        long tm = dt.atZone(zone).toEpochSecond();

        t1Time.setText(String.valueOf(tm));
    }

    @Override
    public String getTabTitle()
    {
        return "UNIX Time";
    }

}
